# Gitlab-runner installation guide
You could either use the shared runners from gitlab or gitlab-runners on your own server.

Customly installed "shell" and "docker" executors startup significantly faster than the "docker+machine" executors on the shared runners.
The average startup time on the "docker-machine" executor is 35 seconds. The average startup time on the custom executors is 3 seconds.

A ci/cd setup that is exemplary for a production setup, installs a "docker" executor for clean ci/cd job environments and a "shell" executor for running producion deployment scripts.

Down below follows an installation script that you can follow step by step to create a similar compatible gitlab-runner configuration.

<pre>
#!/bin/sh
# install docker

## remove any pre-existing versions of docker
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt-get install docker-ce docker-ce-cli containerd.io

# install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# download gitlab-runner service
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
dpkg -i gitlab-runner_amd64.deb

# install gitlab-runner "shell" executor for production
sudo gitlab-runner register

## you will have to set some variables
# gitlab-ci coordinator url = https://gitlab.com/
# token from gitlab         = <get token from website>
# tags                      = shell,production 
# exectutor                 = shell

## create a gitlab-runner and give it sudo permissions to run docker(-compose)
sudo usermod -aG docker gitlab-runner

## change the line underneath "User privilige specification"
sudo nano /etc/sudoers
## gitlab-runner ALL=(ALL) NOPASSWD: ALL

# Install gitlab-runner "docker" executor for ci/cd jobs
sudo gitlab-runner register

## you will have to set some variables
# gitlab-ci coordinator url = https://gitlab.com/
# token from gitlab         = <get token from website>
# tags                      = docker
# exectutor                 = docker

## change the following lines
nano /etc/gitlab-runner/config.toml

## give sudo permissions
### privileged = true

## disable caching in order to always run the latest builds
### disable_cache = true
### remove docker volume /cache line

#restart service to apply changes
sudo gitlab-runner restart
</pre>
