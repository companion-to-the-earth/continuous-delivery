# Continuous delivery
A ci/cd package for companion-to-the-earth python repositories.

## Installation guide
The CI/CD pipeline can be installed by adding the following .gitlab-ci.yml file to your repository and replacing the version entry in the template.
The version entry can tag or a branch, depending on whether you would like to stick with a specific version or the latest version respectively.

<pre>
image: registry.gitlab.com/companion-to-the-earth/continuous-delivery/cd:<VERSION>

include:
  - project: 'companion-to-the-earth/continuous-delivery'
    ref: <VERSION>
    file: '.gitlab-ci.public.yml'

</pre>

### CI/CD enforced repository structure
The CI/CD enforces a specific repository structure through its linter in order to work properly.

<pre>
./
├── image                             #name this directory 'app' in your container
    ├── __init__.py
    ├── lib/
    |   ├── __init__.py
    |   └── lib_example.py
    ├── src/                          #directory container source code
    |   ├── __init__.py
    |   └── example.py
    ├── test_unit/                    #directory containing unit tests 
    |   ├── __init__.py
    |   └── utest_example.py
    ├── test_integration/             #directory containing integration tests
    |   ├── __init__.py
    |   ├── itest_example.py          #integration test file
    |   ├── docker-compose.test.yml   #optional
    |   └── services.txt              #file to specify which services to run
    └── test_end2end/
        ├── __init__.py
        ├── etest_example.py          #end2end test file
        └── docker-compose.test.yml   #optional
</pre>

## How to write tests?
Tests are written using the python unittest library. https://docs.python.org/3/library/unittest.html

### How to write unit-tests
Unit-tests can be easily added by prefixing your unit-test file with "utest_" and adding them to the test_unit directory (View repository structure). For example utest_database.py.

### How to write integration tests
Integration tests can be run either on the container itself or and external container. This depends on your test case.

Testing whether the container itself can send messages may done from the container itself, but testing whether a production ready message sequences between containers works, should probably be done from an external container just like in the production environment.

Another reason for running tests from an external container may be that your container runs a blocking message consumption function, which can leave tests running indefinitely.

By default integration-tests are on the container itself. For this case, a docker-compose.test.yml is generated during run-time. You can specify an external container by adding a docker-compose.test.yml file yourself. This file overrides some parts and extends the default production docker-compose file. For example:

<pre>
version: '3'

services:
  test-container:
    image: python-alpine:3.8
    container_name: test-container
    command: python -m unittest discover ./app/ -p 'itest_*.py'"
</pre>

#### Services.txt
Every test_integration should contain a services.txt file. This file contains the names of all services that you would like to run from the production docker-compose file. This makes sure the testing environment is equal to the production environment while speeding up the test process by reducing the amount of services to run.

A services.txt file should look like this:

<pre>
download
middleware
authentication

</pre>

### How to write end-2-end tests
End-2-end tests are fired at a copy of the production environment from a separate container.

End-2-end tests are written just like unit and integration tests using the python unittest library. Make sure to prefix the end2end test file with 'etest_'.

You should specify the external container you'd like to use in a docker-compose.test.yml file inside the test_end2end directory. Such a file is just like any other docker-compose.yml file, but keep in mind to add the command to trigger the tests. An example of such a file may look like this:

<pre>
version: '3'

services:
  test-container:
    image: python-end2end:1.0.0
    container_name: test-container
    command: python -m unittest discover ./app/ -p 'etest_*.py'"
</pre>
