# Utrecht Companion To The Earth - cd
The cd docker image contains all libraries and scripts to execute the ci/cd pipeline for companion-to-the-earth python repositories.

## image
- Linux alpine
- Python3
- Docker
- Docker-compose
