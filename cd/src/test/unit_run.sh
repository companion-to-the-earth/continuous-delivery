#!/bin/sh
#
# Script that runs unit tests and tracks coverage.

cd app
export COVERAGE_FILE=/.coverage
coverage run --branch -m unittest discover . -p 'utest_*.py'

