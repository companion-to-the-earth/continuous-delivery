#!/bin/sh
#
# Print a test-report.
# The report evaluates the following errors:
#   * Container exit codes
#   * Tests fails
#   * Docker errors
#   * Container registry errors

# Import libraries
. 'cd/src/lib/print.sh'

# Set variables
LOG_FILE="$1"
HEADER="$2"
FAILED_TESTS_FILE="logs_failed.txt"
DOCKER_FAIL_FILE="logs_docker.txt"
REGISTRY_FAIL_FILE="logs_registry.txt"
exit_code=0

# Store all lines that should throw a non-zero exit value
grep "FAILED" "$LOG_FILE" > "$FAILED_TESTS_FILE"
grep "Couldn't connect to Docker daemon" "$LOG_FILE" >> "$DOCKER_FAIL_FILE"
grep "manifest for .*. not found" "$LOG_FILE" >> "$DOCKER_FAIL_FILE"
grep "No such service:.*" "$LOG_FILE" >> "$REGISTRY_FAIL_FILE"

# Check whether errors were raised and sort them in files
count_err_tests=$(wc -l < "$FAILED_TESTS_FILE")
count_err_docker=$(wc -l < "$DOCKER_FAIL_FILE")
count_err_registry=$(wc -l < "$REGISTRY_FAIL_FILE")

# Check if all containers exited succesfully
count_container_all=$(docker ps --all | wc -l)
count_container_exit0=$(docker ps -a --filter=exited=0 | wc -l)

header "$HEADER"

[ ! $count_container_all -eq $count_container_exit0 ] \
  && printf "%s\n%s\n" "Some containers did not exit with code 0." \
  "More info on:
https://success.docker.com/article/
what-causes-a-container-to-exit-with-code-137" \
  && docker ps --all \
  && exit_code=1

# Evaluate logs
[ $count_err_tests -gt 0 ] \
  && printf "\n%s\n" "Some tests failed" \
  && cat "$FAILED_TESTS_FILE" \
  && exit_code=1

[ $count_err_docker -gt 0 ] \
  && printf "\n%s\n" "Something went wrong with docker" \
  && cat "$DOCKER_FAIL_FILE" \
  && exit_code=1

[ $count_err_registry -gt 0 ] \
  && printf "\n%s\n" "Something went wrong with the Container Registry." \
  && cat "$REGISTRY_FAIL_FILE" \
  && exit_code=1

# Clean up
rm "$FAILED_TESTS_FILE"
rm "$DOCKER_FAIL_FILE"
rm "$REGISTRY_FAIL_FILE"

# Exit
[ $exit_code -eq 1 ] && exit 1

echo "All tests were completed succesfully"
exit 0
