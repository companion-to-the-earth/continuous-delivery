#!/bin/sh
#
# Create a docker-compose.test.yml file that runs integration-tests.

# Set variables
IMAGE="$1"
FILEPATH="$IMAGE/test_integration/docker-compose.test.yml"

# Exit if docker-comopose.test.yml file is already present.
[ -f $FILEPATH ] \
  && echo "Execute tests for $IMAGE on external container" \
  && exit 0

echo "build docker-compose.test.yml to turn on testing on image"

echo "version: '3'" > "$FILEPATH"
echo "services:" >> "$FILEPATH"

echo "  $IMAGE:" >> "$FILEPATH"
echo "    command: coverage run --branch -m unittest discover ./app/ -p 'itest_*.py'" >> "$FILEPATH"
