#!/bin/sh
#
# Run end-2-end tests

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/auth.sh'
. 'cd/src/lib/load.sh'

# Set variables
load_env_file '.env'
IMAGE_LIST='images.txt'
E2E_DIR='test_end2end'
ARTIFACT_LOGS='error_logs.txt'
exit_code=0
TIME_OUT=60

# Init environment
# If e2e-tests were not changed, exit if there were no changes to images.
[ ! -f 'run_e2e.txt' ] \
  && exit_if_no_changes "$IMAGE_LIST" \
	|| echo "Found changes in e2e-tests."

verify_dependency 'docker-compose'
login_container_registry

# Run docker-compose in a detached state
STAGE="$CD_STAGE_TEST" docker-compose \
	--file docker-compose.yml up -d | tee -a "$ARTIFACT_LOGS"

# Run tests from separate end-point
STAGE=$CD_STAGE_TEST docker-compose \
	--file "$E2E_DIR/docker-compose.test.yml" \
	up --timeout $TIME_OUT \
	2>&1 | tee -a "$ARTIFACT_LOGS"

#get logs when test is finished
docker-compose --file docker-compose.yml logs >> "$ARTIFACT_LOGS"

#stop in order to get exit code from all running containers
docker-compose --file docker-compose.yml stop

#print report
sh cd/src/test/report.sh "$ARTIFACT_LOGS" "REPORT" > report.txt \
  || exit_code=1

cat report.txt

exit $exit_code
