#!/bin/sh
#
# Run unit-tests on python containers

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/auth.sh'
. 'cd/src/lib/load.sh'

# Set variables
load_env_file '.env'
IMAGE_LIST='images_py_test_unit.txt'
SCRIPT='unit_run.sh'
REPORT_ARTIFACT='report.txt'
ARTIFACT_DIR="artifacts_test_unit"
REPOSITORY_URL="registry.gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
IMAGE_TAG="$CI_COMMIT_REF_NAME-$CD_STAGE_TEST"
exit_value=0

# Init environment
verify_dependency 'docker'
exit_if_no_changes "$IMAGE_LIST"
login_container_registry

mkdir "$ARTIFACT_DIR"

# Remove logs if they exist on the shell executor
[ -f unit_test_logs.txt ] && rm unit_test_logs.txt

# Execute unit tests on all rebuild images when supported
for image in $(cat "$IMAGE_LIST"); do
  header "$image" | tee -a "$REPORT_ARTIFACT"

  image_url="$REPOSITORY_URL/$image:$IMAGE_TAG"
  container_id=$(docker create --entrypoint "sh" "$image_url" /"$SCRIPT")

  # Copy test script into container
  docker cp \
    'cd/src/test/unit_run.sh' \
    $container_id:/"$SCRIPT"

  docker start -i $container_id 2>&1 | tee -a unit_test_logs.txt

  docker cp \
    "$container_id":/.coverage \
    "$ARTIFACT_DIR/.coverage.$image.test.unit"

  # Get report
  container_exit_code=$(docker inspect "$container_id" \
    --format='{{.State.ExitCode}}')
  [ $container_exit_code -eq 0 ] \
    && echo "All unit-tests were succesful" >> "$REPORT_ARTIFACT" \
    && continue

  error "Container exited with code $EXIT_CODE" >> "$REPORT_ARTIFACT" \
    && exit_value=1

done

grep 'FAILED' unit_test_logs.txt \
  && error "some unit-tests failed" \
  | tee -a "$REPORT_ARTIFACT" \
  && exit_value=1

header "Report"
cat "$REPORT_ARTIFACT"

exit $exit_value

