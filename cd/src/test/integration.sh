#!/bin/sh
#
# Run integration tests

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/auth.sh'
. 'cd/src/lib/load.sh'

#set variables
load_env_file '.env'
ARTIFACT_DIR="artifacts_test_integration"
IMAGE_LIST='images_test_integration.txt'
TIME_OUT=60 # default timeout is 10s, middleware needs more time
exit_value=0
export STAGE=$CD_STAGE_TEST

#init environment
exit_if_no_changes "$IMAGE_LIST"
login_container_registry

#init files and directories
mkdir "$ARTIFACT_DIR"
touch test_integration_output.txt
touch temp_test_output.txt

#execute integration tests on all rebuild images when supported
for image in $(cat "$IMAGE_LIST"); do

  header "$image"

  sh 'cd/src/test/create_docker-compose.sh' "$image"

  container_commit_name="coverage/$image"

  #start all required services for this integration test
  STAGE="$CD_STAGE_TEST" docker-compose  \
    --file 'docker-compose.yml' \
    --file "$image/test_integration/docker-compose.test.yml" \
    up --timeout $TIME_OUT \
    "$image-test-integration" \
    2>&1 | tee -a 'temp_test_output.txt'

  # stop all services
  STAGE="$CS_STAGE_TEST" docker-compose \
    --file 'docker-compose.yml' \
    --file "$image/test_integration/docker-compose.test.yml" \
    stop

  # get logs as artifacts, STAGE isn't required here but it avoids an unnecessary warning
  # The reason we don't do --attach-dependencies is because the other containers
  # won't shut down automatically so the pipeline would get stuck. We could
  # prevent this with --abort-on-container-exit but there is a container that's
  # supposed to exit and this would stop all tests. Getting the log files of all
  # other containers and attaching them as artifacts was the easiest solution.
  STAGE="$CD_STAGE_TEST" docker-compose \
    --file 'docker-compose.yml' \
    --file "$image/test_integration/docker-compose.test.yml" \
    logs > "$ARTIFACT_DIR/$image.logs.txt"

  # find container id with coverage data
  container_name="$CI_PROJECT_NAME"_"$image"_1
  container_id=$(docker ps -a \
    --filter=name="$container_name" \
    --format "{{.ID}}")

  # if container id is still empty,
  # try to get coverage from integration test container
  if [ -z "$container_id" ]; then
    container_name="$CI_PROJECT_NAME"_"$image"-test-integration_1
    container_id=$(docker ps -a \
      --filter=name="$container_name" \
      --format "{{.ID}}")
  fi

  docker commit "$container_id" "$container_commit_name"

  # create container from new image
  container_id=$(docker create \
    --workdir / \
    --entrypoint "sh" \
    "$container_commit_name" \
    '/combine.sh')

  # copy combine script to new container
  docker cp \
    'cd/src/coverage/combine.sh' \
    "$container_id:/combine.sh"

  docker start -i "$container_id"

  # copy combined coverage file to ARTIFACT_DIR
  docker cp \
    "$container_id:/.coverage" \
    "$ARTIFACT_DIR/.coverage.$image.test.integration"

  # append temp results to test_output artifact
  cat 'temp_test_output.txt' >> 'test_integration_output.txt'

  # print intermediate report for this image
  sh 'cd/src/test/report.sh' 'temp_test_output.txt' "$image" \
    > 'report.temp.txt' \
    || exit_value=1
  cat 'report.temp.txt' 2>&1 | tee -a 'report.txt'

  #clean up
  docker container stop --time $TIME_OUT $(docker container ls -aq) > /dev/null
  docker container rm $(docker container ls -aq) > /dev/null

done

#print report
printf "%s\n%s\n%s\n\n" \
  "+----------------+" \
  "|  ERROR REPORT  |" \
  "+----------------+"
cat 'report.txt'

exit $exit_value
