#!/bin/sh
#
# A library that verifies the pipeline environment

# Import libraries
. 'cd/src/lib/print.sh'

#######################################
# Exit 1, if dependency was not installed.
# Arguments:
#   Dependency, a string
#######################################
verify_dependency() {
  ! which "$1" > /dev/null \
    && error "Dependency was not found: $1" \
    && exit 1 
}

#######################################
# Exit 0, if no files were changed.
# Exit 1, if image list was not found.
# If files were changed, print the files.
# Arguments:
#   Image list, a file
#######################################
exit_if_no_changes() {

  [ ! -f "$1" ] \
    && echo "$1 was not found" \
    && exit 1

  [ ! -s "$1" ] \
    && echo "No changes were found" \
    && exit 0

  echo "Running script for:"
  summation "$1"
}
