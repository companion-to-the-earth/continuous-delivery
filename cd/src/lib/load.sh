#!/bin/sh
#
# A library for loading pipeline assets

#######################################
# Load a .env file
# Returns exit_code 1 if any value was not set
# Arguments:
#   .env file, a file
#######################################
load_env_file() {
  # Set variables
  ENV_FILE="$1"
  ENV_CHECKLIST='cd/src/lib/cd_var_list.txt'
  exit_code=0

  # Import libraries
  . 'cd/src/lib/print.sh'

  # Verify if ENV_FILE exists
  [ ! -f "$ENV_FILE" ] && error "No $ENV_FILE found" && exit 1

  # Load all environment variables
  export $(grep '^CD_' "$ENV_FILE")

  # Verify if all required ENV_VARS are set in the ENV_FILE
  for env_var in $(cat $ENV_CHECKLIST); do
    [ "$env_var" = '' ] && error "$ENV_VAR was not set." && exit_code=1
  done

  [ $exit_code -eq 1 ] && exit 1
}

#######################################
# Get environment variable value
# Returns exit_code 1 if any value was not set
# Arguments:
#   Environment variable, a string
#   .env file, a file
#######################################
get_env_var() {
  line=$(grep "$1" "$2")
  echo "${line#*=}"
}
