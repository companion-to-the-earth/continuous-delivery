#!/bin/sh
#
# A library for printing
# By using this library, scripts generate consistent output

#######################################
# Print a header to STDOUT
# Arguments:
#   Header, a string
#######################################
header() {
  printf '.\n%s\n' "$1" | awk '{ print toupper($0) }'
}

#######################################
# Print an error message to STDERR
# Arguments:
#   Error message, a string
#######################################  
error() {
  printf 'Error: %s\n' "$1" >&2
}

#######################################
# Print a summation from a file
# Arguments:
#   List of strings, file
#######################################  
summation(){
  printf '* %s\n' $(cat "$1")
}

