#!/bin/sh
#
# A library that provides authentication

#######################################
# Log into Gitlab Container Registry
# Arguments:
#   None, all variables are environment variables
#######################################

login_container_registry() {
  echo "$CI_REGISTRY_PASSWORD" \
    | docker login "$CI_REGISTRY" \
    --username "$CI_REGISTRY_USER" \
    --password-stdin \
    >/dev/null 2>&1
}

