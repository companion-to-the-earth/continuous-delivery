#!/bin/sh

THRESHOLD=60

sh combine.sh

coverage report \
	--show-missing \
	--skip-empty \
	--fail-under $THRESHOLD \
	--omit=*utest_*,*itest_*,*lib/*
