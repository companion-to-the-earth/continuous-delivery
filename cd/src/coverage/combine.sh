#!/bin/sh

# Set variables
COVERAGE_TARGET='.coverage'
COVERAGE_FILES='coverage_files.txt'
FILEPATHS_FILE='files.txt'

#get filepaths in current directory
ls -a > "$FILEPATHS_FILE"
sed --in-place '/^\.coveragerc$/d' "$FILEPATHS_FILE"

#get all .coverage files
grep "\.coverage" "$FILEPATHS_FILE" > "$COVERAGE_FILES"

# Create a single /.coverage file for two cases.
# Case_1: rename a single .coverage file to .coverage.
# Case_2: combine multiple coverage files into a single .coverage file.
coverage_count=$(wc -l < "$COVERAGE_FILES")

echo "Combining .coverage files"
cat "$COVERAGE_FILES"

[ $coverage_count -eq 1 ] \
  && mv $(cat "$COVERAGE_FILES") "$COVERAGE_TARGET" \
  && echo "moved coverage file to $COVERAGE_TARGET"

[ $coverage_count -gt 1 ] \
  && coverage combine $(cat "$COVERAGE_FILES") && \
  echo "combined coverage files into $COVERAGE_TARGET"

exit 0
