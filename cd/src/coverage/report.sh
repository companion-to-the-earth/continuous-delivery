#!/bin/sh

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/auth.sh'
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/load.sh'

#set variables
load_env_file '.env'
IMAGE_LIST='images_py.txt'
REPORT_FILE="code_report.txt"
ARTIFACT_DIR_TEST_UNIT="artifacts_test_unit"
ARTIFACT_DIR_TEST_INTG="artifacts_test_integration"
SCRIPT_ENTRYPOINT='combine.sh'
THRESHOLD=40
exit_code=0

#log into container registry
exit_if_no_changes "$IMAGE_LIST"
verify_dependency 'docker'
login_container_registry
mkdir code_reports

#set variables using env_vars
REPOSITORY_URL="registry.gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
CONTAINER_TAG="$CI_COMMIT_REF_NAME-$CD_STAGE_TEST"

#run code coverage report inside containers
for image in $(cat "$IMAGE_LIST"); do

	image_url="$REPOSITORY_URL/$image:$CONTAINER_TAG"
	coverage_file_prefix=".coverage.$image.test"
	coverage_file_utest="$coverage_file_prefix.unit"
	coverage_file_itest="$coverage_file_prefix.integration"
	coverage_filepath_utest="$ARTIFACT_DIR_TEST_UNIT/$coverage_file_utest"
	coverage_filepath_itest="$ARTIFACT_DIR_TEST_INTG/$coverage_file_itest"

	#print header with name of image
	header "$image" >> "$REPORT_FILE"

	#skip if both no unit tests and integration tests coverage files are present for this image
	[ ! -f "$coverage_filepath_utest" ] \
		&& [ ! -f "$coverage_filepath_itest" ] \
		&& echo "Error: no unit or integration tests were written for $image" >> "$REPORT_FILE" \
		&& exit_code=1 \
		&& continue

	#do not print pull output
	docker pull --quiet "$image_url" > /dev/null

	container_id=$(\
		docker create \
		  --entrypoint 'sh' \
		  "$image_url" \
      'combine_and_print.sh')
		
	workdir=$(docker inspect --format='{{.Config.WorkingDir}}' $container_id)

	#copy scripts into container
	docker cp \
		"cd/src/coverage/combine_and_print.sh" \
		"$container_id:$workdir/combine_and_print.sh"

	docker cp \
		"cd/src/coverage/combine.sh" \
		"$container_id:$workdir/combine.sh"

  docker cp \
		"cd/src/lib/verify.sh" \
		"$container_id:$workdir/verify.sh"

	[ -f "$coverage_filepath_utest" ] &&
		docker cp \
		"$coverage_filepath_utest" \
		"$container_id:$workdir/$coverage_file_utest"

	[ -f "$coverage_filepath_itest" ] &&
		docker cp \
		"$coverage_filepath_itest" \
		"$container_id:$workdir/$coverage_file_itest"

	docker start \
		--interactive \
		$container_id >> "$REPORT_FILE" || exit_code=1
done

#print report
[ -f "$REPORT_FILE" ] \
  && cat "$REPORT_FILE" \
	|| echo "No .coverage files were found"

#exit
[ $exit_code -eq 1 ] \
  && echo "Not all services meet the code coverage threshold of $THRESHOLD"
      
exit $exit_code
