#!/bin/sh
#
# Lint a docker-compose file.

# Import libraries.
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/load.sh'

# Set variables.
COMPOSE_FILE="$1"
ERROR_ARTIFACT='docker-compose_lint.txt'

# Init environment.
verify_dependency 'docker-compose'
load_env_file '.env'

# Skip linting if docker-compose.yml file not found.
[ ! -f "$COMPOSE_FILE" ] \
  && echo "No $COMPOSE_FILE found" \
  && exit 1

# Lint the docker-compose file.
STAGE="$CD_STAGE_TEST" docker-compose \
  --file "$COMPOSE_FILE" \
  config \
  2> "$ERROR_ARTIFACT" \
  1>/dev/null

#print error-report.
cat "$ERROR_ARTIFACT"

#exit
if [ -s "$ERROR_ARTIFACT" ]; then
  exit 1
fi
