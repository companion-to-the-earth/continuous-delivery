#!/bin/sh
#
# Lint the repository.
# Return exit-code 1, if incompatible with ci-cd pipeline.

verify_init () {
  [ -d $1 ] \
    && [ "$(find $1 -name '*.py')" ] \
    && [ ! -f $1/__init__.py ] \
    && error "__init__.py missing in $1" \
    && return 1
  return 0
}

verify_subdirectories () {
  [ -d $1 ] \
    && [ "$(ls -l $1 | grep '^d' | awk '{ print $9 }')" ] \
    && error "Subdirectories are not allowed in $1" \
    && return 1
  return 0
}

file_ends_with_newline() {
  [ $(tail -c1 "$1" | wc -l) -gt 0 ] && return 0 || return 1
}

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/verify.sh'

# Set variables
IMAGE_LIST='images.txt'
warning_total=0
error_total=0

# Init environment
exit_if_no_changes "$IMAGE_LIST"

while read -r image; do
  # Initialize linter
  warning=0
  error=0
  tests=5

  # Start linting
  header "$image"

  # Warning: no README.md as added to image directory
  [ ! -f $image/README.md ] \
    && echo "W: No README.md file was added to $image" \
    && warning=$(($warning+1))

  # Warning: disallowed files in root of image.
  # If other files present in root of image,
  # then something fishy may be going on
  find $image -maxdepth 1 -type f > files.txt
  while read -r file
  do
    # Prefix allowed_filepaths with container image name
    printf "$image/%s\n" $(cat cd/src/lint/allowed_files.txt) \
      > allowed_files.txt

    #check if disallowed files are present in image
    [ ! $(grep $file allowed_files.txt) ] \
      && echo "W: $file is not accepted in the image directory" \
      && warning=$(($warning+1))
  done < files.txt

  # Error: if python files exist in image then __init__.py must be present.
  verify_init $image || error=$(($error+1))

  # Error: if python files exist in src then __init__.py must be present.
  verify_init $image/src || error=$(($error+1))

  # Check if unit tests are present.
  if [ -d $image/test_unit ]
  then
    tests=$(($tests+2))

    #Error: if .py files exist in test_unit
    #       then __init__.py must be present.
    verify_init $image/test_unit || error=$(($error+1))

    # Error: found disallowed subdirectories in test_unit subdirectory.
    verify_subdirectories $image/test_unit || error=$(($error+1))
  fi

  # Check if integration tests are present.
  if [ -d $image/test_integration ]
  then
    tests=$(($tests+5))

    # Error: if .py files exist in test_integration
    #        then __init__.py must be present.
    verify_init $image/test_integration || error=$(($error+1))

    # Error: found disallowed subdirectories in test_integration directory.
    verify_subdirectories $image/test_integration || error=$(($error+1))
  fi

  if [ -d $image/test_end2end ]
  then
    tests=$(($tests+3))

    # Error: if .py files exist in test_end2end
    #        then __init__.py must be present.
    verify_init $image/test_end2end || error=$(($error+1))

    # Error: found disallowed subdirectories in test_end2end subdirectory.
    verify_subdirectories $image/test_end2end || error=$(($error+1))

    # Error: no docker-compose.test.yml present.
    [ ! -f $image/test_end2end/docker-compose.test.yml ] \
      && echo "E: no docker-compose.test.yml found in $image/test_end2end" \
      && error=$(($error+1))
  fi

  # Print results.
  echo "---"
  echo "Ran $tests tests: Found $error errors and $warning warnings."

  # Update global variables.
  warning_global=$(($warning_global+$warning))
  error_global=$(($error_global+$error))

done < "$IMAGE_LIST"

# Exit with exit-code 1 if any errors occured, warnings are ignored.
[ $error_global -gt 0 ] && exit 1 || exit 0
