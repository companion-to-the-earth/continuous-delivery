#!/bin/sh
#
# Lints python code and returns exit-code 1 if the threshold is not met.
# The highest possible score is a 10.

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/auth.sh'
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/load.sh'

#set variables
load_env_file '.env'
THRESHOLD=8
IMAGE_LIST='images_py.txt'
OUTPUT_TEMP='code_style_analysis_temp.txt'
OUTPUT_ARTF='code_style_analysis.txt'
REPOSITORY_URL="registry.gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
CONTAINER_TAG="$CI_COMMIT_REF_NAME-$CD_STAGE_TEST"
exit_value=0

# Init environent
exit_if_no_changes "$IMAGE_LIST"
verify_dependency 'docker'
login_container_registry

header 'Linting services:'

# Review code-style of changed files in each service 
for image in $(cat "$IMAGE_LIST"); do

  image_url="$REPOSITORY_URL/$image:$CONTAINER_TAG"

  #define container
  container_id=$(\
    docker create \
    --entrypoint "pylint" \
    "$image_url" \
    --rcfile=/.pylintrc \
    app)

  # Copy config file into container
  docker cp .config/.pylintrc $container_id:/.pylintrc

  #run pylint inside container
  docker start -i $container_id | tee "$OUTPUT_TEMP" >> "$OUTPUT_ARTF"

  #get report analysis and print result
  sh 'cd/src/lint/codestyle.report.sh' "$OUTPUT_TEMP" > score \
    || exit_value=1

  printf "* $image: %s\n" $(cat score)

done

exit $exit_value

