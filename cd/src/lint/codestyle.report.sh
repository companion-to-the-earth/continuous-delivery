#!/bin/sh
#
# Analyse codestyle report and extract score

# Import libraries
. 'cd/src/lib/print.sh'

# Set variables
CODESTYLE_REPORT="$1"
THRESHOLD=8

# Check for errors during the code style analysis.   
# These errors would be caused by a broken container.
grep --quiet 'Errno' "$CODESTYLE_REPORT" \
  && error "Unknown system error" \
  && exit 1                          

# Extract and display score using file
score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' "$CODESTYLE_REPORT")

# Continue to next image if no score was given
[ -z "$score" ] \
  && error "Pylint could not assign a score" \
  && exit 1

# Set exitvalue=1 if threshold was not met
integer_score="${score%%.*}"
[ $integer_score -lt $THRESHOLD ] \
  && exit 1

# Print score of image
echo "$score"
exit 0
