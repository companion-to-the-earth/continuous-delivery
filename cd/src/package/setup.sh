#!/bin/sh
#
# Set up docker container to build package

# Import libraries
. 'cd/src/lib/print.sh'

# Set variables
PACKAGE_LIST=".config/.packages"
TIME_OUT=20
TWINE_USERNAME="gitlab-ci-token"
TWINE_PASSWORD="${CI_JOB_TOKEN}"
REPOSITORY_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi"

if [ ! -f .config/.packages ]; then
    error "Nothing to package, list all packages in \".config/.packages\" or remove this step"
    exit 1
fi

echo "Running script for:"
summation "$PACKAGE_LIST"

for package in $(cat "$PACKAGE_LIST"); do

    header "$package"

    # Verify setup.py is present
    if [ ! -f "$package/setup.py" ]; then
        error "setup.py missing"
        exit 1
    fi

    # Create docker container for packaging
    container_id=$(docker create \
        --workdir "/$package" \
        --entrypoint "sh" \
        python:latest \
        "package.sh")

    # Copy package dir and packaging script
    docker cp "$package" "$container_id:/"
    docker cp "cd/src/package/package.sh" "$container_id:/$package/package.sh"
    docker commit "$container_id" "package/$package"

    # Run in packaging container, pass environment variables
    docker run --env TWINE_USERNAME="$TWINE_USERNAME" \
               --env TWINE_PASSWORD="$TWINE_PASSWORD" \
               --env REPOSITORY_URL="$REPOSITORY_URL" \
               -i \
               package/"$package"

    # Clean up
    docker container stop --time $TIME_OUT $(docker container ls -aq) > /dev/null
    docker container rm $(docker container ls -aq) > /dev/null

 done