#!/bin/sh
#
# Build python package and upload to gitlab package registry

# Install dependencies
pip install twine

# Build package
python setup.py sdist bdist_wheel

# Upload to package registry
python -m twine upload --repository-url "${REPOSITORY_URL}" dist/*