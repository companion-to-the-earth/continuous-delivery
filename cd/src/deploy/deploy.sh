#!/bin/sh
#
# Deploy software using adocker-compose.yml and an .env file.

# Import libraries
. 'cd/src/lib/auth.sh'

# Set variables
STAGE="$1"
CLONE_PATH="repo"
CLONE_URL="https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME.git"

# Init environment
login_container_registry

#get newest docker-compose file, add an mv statement if you need additional files
[ -d "$CLONE_PATH" ] && rm -rf "$CLONE_PATH"
git clone \
	--single-branch \
	--branch "$CI_COMMIT_TAG"  \
	"$CLONE_URL" \
	"$CLONE_PATH"

mv "$CLONE_PATH"/docker-compose.yml docker-compose.yml

# execute predeploy script if it exists
if [ -f "$CLONE_PATH"/.config/predeploy.sh ]; then
	. "$CLONE_PATH"/.config/predeploy.sh
fi

rm -rf "$CLONE_PATH"

#spin-up the production environment. Uses a production .env file on the server.
STAGE="$STAGE" docker-compose -f docker-compose.yml up -d
