#!/bin/sh

OLD_SHA=$1

#verify whether last sha from previous push exists
! $(git cat-file -e "$OLD_SHA" 2> /dev/null) &&
  echo "Old sha not found: $OLD_SHA" &&
  echo "Build all images for $OLD_SHA" &&

  #rebuild everything
  ls > changedfiles.txt &&
  exit 0

# if FORCE_REBUILD is set, rebuild everything
if [ "${FORCE_REBUILD+1}" = "1" ]; then
  ls > changedfiles.txt
  exit 0
fi

#but if it exists, only rebuild changes
git diff --name-only "$OLD_SHA" "$CI_COMMIT_SHA" > changedfiles.txt
