#!/bin/sh
#
# Build containers from images.
# Then push them to the Gitlab Container Registry.

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/auth.sh'
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/load.sh'

# Set variables
load_env_file '.env'
STAGE="$1"
IMAGE_LIST='images.txt'
REPOSITORY_URL="registry.gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
CONTAINER_TAG="$CI_COMMIT_REF_NAME-$STAGE"

# Init environment
exit_if_no_changes "$IMAGE_LIST"
verify_dependency 'docker'

header "Building containers:"

# Build and push the production images to the Gitlab Container Registry
# Tag them by ref_name
for image in $(cat "$IMAGE_LIST"); do
  # Init
  image_url="$REPOSITORY_URL/$image:$CONTAINER_TAG"
  header "$image"
  login_container_registry

  # Build 
  docker build \
    --quiet \
    --build-arg STAGE="$STAGE" \
    --build-arg CD_STAGE_TEST="$CD_STAGE_TEST" \
    --tag "$image_url" \
    "$image/"

  # Push
  docker push "$image_url" > /dev/null
done

