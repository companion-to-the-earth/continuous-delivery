#!/bin/sh
#
# Each directory in the repository represents and image.
# Therefore, any change inside that directory changes the respective image.
# This script generates files with changed images
#
# Artifacts:
#   * images.txt                  - changed images.
#   * images_py.txt               - changed python images.
#   * images_py_test_unit.txt     - changed python images with unit-tests.
#   * images_test_integration.txt - changed images with integration-tests.
#   * images_test_e2e.txt         - changed images with end-2-end-tests.


#######################################
# Print whether a dir container another dir.
# Example:
# Arguments:
#   Path-list, a file with paths.
#   Sub-directory, a directory.
#######################################
contains_directory() {
  while read -r path; do
    [ -d $path ] && [ -d $path/$2 ] && echo "$path"
  done < "$1"
}

# Import libraries
. 'cd/src/lib/print.sh'

# Init environment
touch directories.txt
touch unique_directories.txt
touch images.txt
touch images_py.txt
touch images_py_test_unit.txt
touch images_test_integration.txt
touch images_test_e2e.txt

# Rebuild everything some files were changed
[ "$(grep gitlab-ci changedfiles.txt)" ] \
  || [ "$(grep docker-compose.yml changedfiles.txt)" ] \
  || [ "$(grep \.env changedfiles.txt)" ] \
  && ls > changedfiles.txt

# If tests were changed create run_e2e.txt and remove it from images.txt
header 'End2end tests changed:'
grep 'test_end2end' changedfiles.txt && echo 'E2E_TESTS_CHANGED=1' > run_e2e.txt
sed -i '/test_end2end/d' changedfiles.txt

# Print which directories were changed
header "Files that trigger rebuild of image:"
while read -r filepath; do

  # Display whether file is ignored or not
  if grep -Fxq "$filepath" .config/.buildignore; then
    echo "X $filepath"
  else
    echo "V $filepath"
    echo "${filepath%%/*}" >> directories.txt
  fi

done < changedfiles.txt

# Remove all duplicates and extract all unique directories
awk '!a[$0]++' directories.txt > unique_directories.txt

# Verify whether the paths are images
while read -r path; do
  [ -d "$path" ] && [ -f "$path/Dockerfile" ] && echo "$path" >> images.txt
done < unique_directories.txt

# Summarize
header 'Images to rebuild:'
summation 'images.txt'

# Get python images
while read -r image; do
  [ ! -d $image/src ] && continue

  ls $image/src > files.txt
  grep --quiet '\.py$' 'files.txt' && echo "$image" >> 'images_py.txt'
done < 'images.txt'

# Get python images with unit-tests
contains_directory 'images_py.txt' 'test_unit' > 'images_py_test_unit.txt'

# Get images with integration-tests
contains_directory 'images.txt' 'test_integration' \
  > 'images_test_integration.txt'

# Get images with end2end-tests
contains_directory 'images.txt' 'test_end2end'  > 'images_test_e2e.txt'

# Clean up
rm 'directories.txt'
rm 'unique_directories.txt'
