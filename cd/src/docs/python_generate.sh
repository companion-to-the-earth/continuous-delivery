#!/bin/sh

cd app
ls src | grep '\.py$' > files.txt

#generate pydoc for each file in module
for file in $(cat files.txt); do
  pydoc "src/$file" >> '/docs.txt'
done
