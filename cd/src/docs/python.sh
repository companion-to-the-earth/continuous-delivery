#!/bin/sh
#
# Generate Python documentation using inline doc-strings.

# Import libraries
. 'cd/src/lib/print.sh'
. 'cd/src/lib/auth.sh'
. 'cd/src/lib/verify.sh'
. 'cd/src/lib/load.sh'

# Set variables
load_env_file '.env'
DOCS_DIR="/docs.txt"
REPOSITORY_URL="registry.gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
IMAGE_TAG="$CI_COMMIT_REF_NAME-$CD_STAGE_TEST"
IMAGE_LIST='images_py.txt'
SCRIPT_DOC_GEN='/python_generate.sh'

# Init environmenta
exit_if_no_changes "$IMAGE_LIST"
mkdir docs

for image in $(cat "$IMAGE_LIST"); do

  header "$image"
  login_container_registry
  
  image_url="$REPOSITORY_URL/$image:$IMAGE_TAG"

  docker pull --quiet "$image_url" > /dev/null
  container_id=$(docker create \
    --entrypoint "sh" \
    "$image_url" \
    "$SCRIPT_DOC_GEN")

  docker cp \
  "cd/src/docs/python_generate.sh" \
  "$container_id:$SCRIPT_DOC_GEN"

  docker start -i $container_id
  
  docker cp \
  "$container_id":"$DOCS_DIR" \
  "docs/$image.txt"

done

